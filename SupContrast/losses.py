"""
Author: Yonglong Tian (yonglong@mit.edu)
Date: May 07, 2020
"""
from __future__ import print_function

import torch
import torch.nn as nn


class SupConLoss(nn.Module):
    """Supervised Contrastive Learning: https://arxiv.org/pdf/2004.11362.pdf.
    It also supports the unsupervised contrastive loss in SimCLR"""
    def __init__(self, temperature=0.07, contrast_mode='all',
                 base_temperature=0.07):
        super(SupConLoss, self).__init__()
        self.temperature = temperature
        self.contrast_mode = contrast_mode
        self.base_temperature = base_temperature

    def forward(self, features, labels=None, mask=None):
        """Compute loss for model. If both `labels` and `mask` are None,
        it degenerates to SimCLR unsupervised loss:
        https://arxiv.org/pdf/2002.05709.pdf

        Args:
            features: hidden vector of shape [bsz, n_views, ...].
            labels: ground truth of shape [bsz].
            mask: contrastive mask of shape [bsz, bsz], mask_{i,j}=1 if sample j
                has the same class as sample i. Can be asymmetric.
        Returns:
            A loss scalar.
        """
        device = (torch.device('cuda')
                  if features.is_cuda
                  else torch.device('cpu'))

        if len(features.shape) < 3:
            raise ValueError('`features` needs to be [bsz, n_views, ...],'
                             'at least 3 dimensions are required')
        if len(features.shape) > 3:
            features = features.view(features.shape[0], features.shape[1], -1)

        batch_size = features.shape[0]
        if labels is not None and mask is not None:
            raise ValueError('Cannot define both `labels` and `mask`')
        elif labels is None and mask is None:
            mask = torch.eye(batch_size, dtype=torch.float32).to(device)
        elif labels is not None:
            labels = labels.contiguous().view(-1, 1)
            if labels.shape[0] != batch_size:
                raise ValueError('Num of labels does not match num of features')
            mask = torch.eq(labels, labels.T).float().to(device)
        else:
            mask = mask.float().to(device)

        contrast_count = features.shape[1]
        contrast_feature = torch.cat(torch.unbind(features, dim=1), dim=0)
        if self.contrast_mode == 'one':
            anchor_feature = features[:, 0]
            anchor_count = 1
        elif self.contrast_mode == 'all':
            anchor_feature = contrast_feature
            anchor_count = contrast_count
        else:
            raise ValueError('Unknown mode: {}'.format(self.contrast_mode))

        # compute logits
        anchor_dot_contrast = torch.div(
            torch.matmul(anchor_feature, contrast_feature.T),
            self.temperature)
        # for numerical stability
        logits_max, _ = torch.max(anchor_dot_contrast, dim=1, keepdim=True)
        logits = anchor_dot_contrast - logits_max.detach()

        # tile mask
        mask = mask.repeat(anchor_count, contrast_count)
        # mask-out self-contrast cases
        logits_mask = torch.scatter(
            torch.ones_like(mask),
            1,
            torch.arange(batch_size * anchor_count).view(-1, 1).to(device),
            0
        )
        mask = mask * logits_mask

        # compute log_prob
        exp_logits = torch.exp(logits) * logits_mask
        log_prob = logits - torch.log(exp_logits.sum(1, keepdim=True))

        # compute mean of log-likelihood over positive
        mean_log_prob_pos = (mask * log_prob).sum(1) / mask.sum(1)

        # loss
        loss = - (self.temperature / self.base_temperature) * mean_log_prob_pos
        loss = loss.view(anchor_count, batch_size).mean()

        return loss


class SupConMoCoLoss(nn.Module):
    """Supervised Contrastive Learning: https://arxiv.org/pdf/2004.11362.pdf.
    It also supports the unsupervised contrastive loss in SimCLR"""
    def __init__(self, contrast_mode, temperature=0.07,
                 base_temperature=0.07):
        super(SupConMoCoLoss, self).__init__()
        self.temperature = temperature
        self.contrast_mode = contrast_mode
        self.base_temperature = base_temperature

    def forward(self, base_features, momentum_features, queue_features, labels, queue_labels):
        """Compute loss for model. If both `labels` and `mask` are None,
        it degenerates to SimCLR unsupervised loss:
        https://arxiv.org/pdf/2002.05709.pdf

        Args:
            features: hidden vector of shape [bsz, n_views, ...].
            labels: ground truth of shape [bsz].
            mask: contrastive mask of shape [bsz, bsz], mask_{i,j}=1 if sample j
                has the same class as sample i. Can be asymmetric.
        Returns:
            A loss scalar.
        """
        device = (torch.device('cuda')
                  if base_features.is_cuda
                  else torch.device('cpu'))

        if self.contrast_mode not in ['queue_all', 'queue_neg']:
            raise ValueError('Unknown mode: {}'.format(self.contrast_mode))

        if len(base_features.shape) < 3:
            raise ValueError('`features` needs to be [bsz, n_views, ...],'
                             'at least 3 dimensions are required')
        if len(base_features.shape) > 3:
            base_features = base_features.view(base_features.shape[0], base_features.shape[1], -1)

        batch_size = base_features.shape[0]
        labels = labels.contiguous().view(-1, 1)
        if labels.shape[0] != batch_size:
            raise ValueError('Num of labels does not match num of features')
        mask = torch.eq(labels, labels.T).float().to(device)

        key_count = momentum_features.shape[1]
        key_feature = torch.cat(torch.unbind(momentum_features, dim=1), dim=0)

        contrast_count = key_count
        contrast_feature = torch.cat(torch.unbind(queue_features, dim=1), dim=0)
        contrast_labels = queue_labels.contiguous().view(-1, 1)
        contrast_mask = torch.eq(labels, contrast_labels.T).float().detach()

        anchor_feature = torch.cat(torch.unbind(base_features, dim=1), dim=0)
        anchor_count = key_count

        # compute logits
        anchor_dot_key = torch.div(
            torch.matmul(anchor_feature, key_feature.T),
            self.temperature)
        anchor_dot_contrast = torch.div(
            torch.matmul(anchor_feature, contrast_feature.T),
            self.temperature)
        # for numerical stability
        key_logits_max, _ = torch.max(anchor_dot_key, dim=1, keepdim=True)
        contrast_logits_max, _ = torch.max(anchor_dot_contrast, dim=1, keepdim=True)
        key_logits = anchor_dot_key - key_logits_max.detach()
        contrast_logits = anchor_dot_contrast - contrast_logits_max.detach()

        # tile mask
        mask = mask.repeat(anchor_count, key_count)
        contrast_mask = contrast_mask.repeat(anchor_count, contrast_count)
        # mask-out self-contrast cases
        logits_mask = torch.scatter(
            torch.ones_like(mask),
            1,
            torch.arange(batch_size * anchor_count).view(-1, 1).to(device),
            0
        )
        mask = mask * logits_mask

        # compute log_prob
        if self.contrast_mode == 'queue_all':
            contrast_exp_logits = torch.exp(contrast_logits) 
            neg_exp_logits = torch.cat([
                torch.exp(key_logits) * logits_mask,
                contrast_exp_logits
            ], dim=1)
            log_prob = torch.cat([key_logits, contrast_logits], dim=1) - torch.log(neg_exp_logits.sum(1, keepdim=True))
            all_mask = torch.cat([mask, contrast_mask], dim=1)
            mean_log_prob_pos = (all_mask * log_prob).sum(1) / all_mask.sum(1)
        elif self.contrast_mode == 'queue_neg':
            contrast_exp_logits = torch.exp(contrast_logits) * (contrast_mask == 0).float().detach()
            neg_exp_logits = torch.cat([
                torch.exp(key_logits) * logits_mask,
                contrast_exp_logits
            ], dim=1)
            log_prob = key_logits - torch.log(neg_exp_logits.sum(1, keepdim=True))
            mean_log_prob_pos = (mask * log_prob).sum(1) / mask.sum(1)
        

        # compute mean of log-likelihood over positive

        # loss
        loss = - (self.temperature / self.base_temperature) * mean_log_prob_pos
        loss = loss.view(anchor_count, batch_size).mean()

        return loss


class SupConMoCoLoss_(nn.Module):
    """Supervised Contrastive Learning: https://arxiv.org/pdf/2004.11362.pdf.
    It also supports the unsupervised contrastive loss in SimCLR"""
    def __init__(self, temperature=0.07,
                 base_temperature=0.07):
        super(SupConMoCoLoss, self).__init__()
        self.temperature = temperature
        self.base_temperature = base_temperature

    def forward(self, base_enc_feats, momentum_enc_feats, labels):
        """Compute loss for model. If both `labels` and `mask` are None,
        it degenerates to SimCLR unsupervised loss:
        https://arxiv.org/pdf/2002.05709.pdf

        Args:
            features: hidden vector of shape [bsz, n_views, ...].
            labels: ground truth of shape [bsz].
            mask: contrastive mask of shape [bsz, bsz], mask_{i,j}=1 if sample j
                has the same class as sample i. Can be asymmetric.
        Returns:
            A loss scalar.
        """
        device = (torch.device('cuda')
                  if base_enc_feats.is_cuda
                  else torch.device('cpu'))

        if (len(base_enc_feats.shape) < 2) or (len(momentum_enc_feats.shape) < 2):
            raise ValueError('`features` needs to be [bsz, ...],'
                             'at least 2 dimensions are required')
        # if base_enc_feats.shape[1] != 2 or momentum_enc_feats.shape[1] != 2:
        #     raise ValueError('n_views should be 2')

        anchor_feature = base_enc_feats.view(base_enc_feats.shape[0], -1)
        contrast_feature = momentum_enc_feats.view(momentum_enc_feats.shape[0], -1)

        batch_size = labels.shape[0]

        labels = labels.contiguous().view(-1, 1)
        if labels.shape[0] != batch_size:
            raise ValueError('Num of labels does not match num of features')
        mask = torch.eq(labels, labels.T).float().to(device)

        anchor_count = 1
        # contrast_count = 1

        # contrast_count = momentum_enc_feats.shape[1]
        # contrast_feature = torch.unbind(momentum_enc_feats, dim=1)
        # anchor_count = base_enc_feats.shape[1]
        # anchor_feature = torch.cat(torch.unbind(base_enc_feats, dim=1), dim=0)

        # compute logits
        # anchor_dot_anchor = torch.div(
        #     torch.matmul(anchor_feature, anchor_feature.T),
        #     self.temperature)
        # contrast_dot_contrast = torch.div(
        #     torch.matmul(contrast_feature, contrast_feature.T),
        #     self.temperature)
        anchor_dot_contrast = torch.div(
            torch.matmul(anchor_feature, contrast_feature.T),
            self.temperature)
        # for numerical stability
        logits_max, _ = torch.max(anchor_dot_contrast, dim=1, keepdim=True)
        # contrast_logits_max, _ = torch.max(contrast_dot_contrast, dim=1, keepdim=True)
        # logits_max, _ = torch.max(torch.cat((anchor_logits_max, contrast_logits_max), dim=1), dim=1, keepdim=True)
        # logits = anchor_dot_contrast - logits_max.detach()
        logits = anchor_dot_contrast - logits_max.detach()
        # contrast_logits = contrast_dot_contrast - logits_max.detach()

        # tile mask
        # mask = mask.repeat(anchor_count, contrast_count)
        # mask-out self-contrast cases
        contrast_logits_mask = torch.scatter(
            torch.ones_like(mask),
            1,
            torch.arange(batch_size * anchor_count).view(-1, 1).to(device),
            0
        )
        # mask = mask * contrast_logits_mask

        # compute log_prob
        contrast_exp_logits = torch.exp(logits) * contrast_logits_mask
        log_prob = logits - torch.log(contrast_exp_logits.sum(1, keepdim=True))

        # compute mean of log-likelihood over positive
        mean_log_prob_pos = (mask * log_prob).sum(1) / mask.sum(1)

        print((mean_log_prob_pos > 0).any())

        # loss
        loss = - (self.temperature / self.base_temperature) * mean_log_prob_pos
        loss = loss.view(anchor_count, batch_size).mean()

        return loss
