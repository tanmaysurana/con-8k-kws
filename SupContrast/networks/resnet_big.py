"""ResNet in PyTorch.
ImageNet-Style ResNet
[1] Kaiming He, Xiangyu Zhang, Shaoqing Ren, Jian Sun
    Deep Residual Learning for Image Recognition. arXiv:1512.03385
Adapted from: https://github.com/bearpaw/pytorch-classification
"""
from functools import partial
import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision
from ..losses import SupConMoCoLoss
from torch.nn import Identity

class BasicBlock(nn.Module):
    expansion = 1

    def __init__(self, in_planes, planes, stride=1, is_last=False):
        super(BasicBlock, self).__init__()
        self.is_last = is_last
        self.conv1 = nn.Conv2d(in_planes, planes, kernel_size=3, stride=stride, padding=1, bias=False)
        self.bn1 = nn.BatchNorm2d(planes)
        self.conv2 = nn.Conv2d(planes, planes, kernel_size=3, stride=1, padding=1, bias=False)
        self.bn2 = nn.BatchNorm2d(planes)

        self.shortcut = nn.Sequential()
        if stride != 1 or in_planes != self.expansion * planes:
            self.shortcut = nn.Sequential(
                nn.Conv2d(in_planes, self.expansion * planes, kernel_size=1, stride=stride, bias=False),
                nn.BatchNorm2d(self.expansion * planes)
            )

    def forward(self, x):
        out = F.relu(self.bn1(self.conv1(x)))
        out = self.bn2(self.conv2(out))
        out += self.shortcut(x)
        preact = out
        out = F.relu(out)
        if self.is_last:
            return out, preact
        else:
            return out


class Bottleneck(nn.Module):
    expansion = 4

    def __init__(self, in_planes, planes, stride=1, is_last=False):
        super(Bottleneck, self).__init__()
        self.is_last = is_last
        self.conv1 = nn.Conv2d(in_planes, planes, kernel_size=1, bias=False)
        self.bn1 = nn.BatchNorm2d(planes)
        self.conv2 = nn.Conv2d(planes, planes, kernel_size=3, stride=stride, padding=1, bias=False)
        self.bn2 = nn.BatchNorm2d(planes)
        self.conv3 = nn.Conv2d(planes, self.expansion * planes, kernel_size=1, bias=False)
        self.bn3 = nn.BatchNorm2d(self.expansion * planes)

        self.shortcut = nn.Sequential()
        if stride != 1 or in_planes != self.expansion * planes:
            self.shortcut = nn.Sequential(
                nn.Conv2d(in_planes, self.expansion * planes, kernel_size=1, stride=stride, bias=False),
                nn.BatchNorm2d(self.expansion * planes)
            )

    def forward(self, x):
        out = F.relu(self.bn1(self.conv1(x)))
        out = F.relu(self.bn2(self.conv2(out)))
        out = self.bn3(self.conv3(out))
        out += self.shortcut(x)
        preact = out
        out = F.relu(out)
        if self.is_last:
            return out, preact
        else:
            return out


class ResNet(nn.Module):
    def __init__(self, block, num_blocks, in_channel=3, zero_init_residual=False):
        super(ResNet, self).__init__()
        self.in_planes = 64

        self.conv1 = nn.Conv2d(in_channel, 64, kernel_size=3, stride=1, padding=1,
                               bias=False)
        self.bn1 = nn.BatchNorm2d(64)
        self.layer1 = self._make_layer(block, 64, num_blocks[0], stride=1)
        self.layer2 = self._make_layer(block, 128, num_blocks[1], stride=2)
        self.layer3 = self._make_layer(block, 256, num_blocks[2], stride=2)
        self.layer4 = self._make_layer(block, 512, num_blocks[3], stride=2)
        self.avgpool = nn.AdaptiveAvgPool2d((1, 1))

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
            elif isinstance(m, (nn.BatchNorm2d, nn.GroupNorm)):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)

        # Zero-initialize the last BN in each residual branch,
        # so that the residual branch starts with zeros, and each residual block behaves
        # like an identity. This improves the model by 0.2~0.3% according to:
        # https://arxiv.org/abs/1706.02677
        if zero_init_residual:
            for m in self.modules():
                if isinstance(m, Bottleneck):
                    nn.init.constant_(m.bn3.weight, 0)
                elif isinstance(m, BasicBlock):
                    nn.init.constant_(m.bn2.weight, 0)

    def _make_layer(self, block, planes, num_blocks, stride):
        strides = [stride] + [1] * (num_blocks - 1)
        layers = []
        for i in range(num_blocks):
            stride = strides[i]
            layers.append(block(self.in_planes, planes, stride))
            self.in_planes = planes * block.expansion
        return nn.Sequential(*layers)

    def forward(self, x, layer=100):
        out = F.relu(self.bn1(self.conv1(x)))
        out = self.layer1(out)
        out = self.layer2(out)
        out = self.layer3(out)
        out = self.layer4(out)
        out = self.avgpool(out)
        out = torch.flatten(out, 1)
        return out


def resnet18(**kwargs):
    return ResNet(BasicBlock, [2, 2, 2, 2], **kwargs)


def resnet34(**kwargs):
    return ResNet(BasicBlock, [3, 4, 6, 3], **kwargs)


def resnet50(**kwargs):
    return ResNet(Bottleneck, [3, 4, 6, 3], **kwargs)


def resnet101(**kwargs):
    return ResNet(Bottleneck, [3, 4, 23, 3], **kwargs)


def efficientnetB2(bn_splits):
    norm_layer = partial(SplitBatchNorm, num_splits=bn_splits) if bn_splits > 1 else nn.BatchNorm2d
    model = torchvision.models.efficientnet_b2(pretrained=False, norm_layer=norm_layer)
    model.features[0][0] = nn.Conv2d(1, 32, 3, 2, 1) # Single channel first convolution
    model.classifier[1] = Identity() # remove classifier for SupCon
    return model


model_dict = {
    'resnet18': [resnet18, 512],
    'resnet34': [resnet34, 512],
    'resnet50': [resnet50, 2048],
    'resnet101': [resnet101, 2048],
    'efficientnetb2': [efficientnetB2, 1408]
}


class SplitBatchNorm(nn.BatchNorm2d):
    def __init__(self, num_features, num_splits, **kw):
        super().__init__(num_features, **kw)
        self.num_splits = num_splits
        
    def forward(self, input):
        N, C, H, W = input.shape
        if self.training or not self.track_running_stats:
            running_mean_split = self.running_mean.repeat(self.num_splits)
            running_var_split = self.running_var.repeat(self.num_splits)
            outcome = nn.functional.batch_norm(
                input.view(-1, C * self.num_splits, H, W), running_mean_split, running_var_split, 
                self.weight.repeat(self.num_splits), self.bias.repeat(self.num_splits),
                True, self.momentum, self.eps).view(N, C, H, W)
            self.running_mean.data.copy_(running_mean_split.view(self.num_splits, C).mean(dim=0))
            self.running_var.data.copy_(running_var_split.view(self.num_splits, C).mean(dim=0))
            return outcome
        else:
            return nn.functional.batch_norm(
                input, self.running_mean, self.running_var, 
                self.weight, self.bias, False, self.momentum, self.eps)


class LinearBatchNorm(nn.Module):
    """Implements BatchNorm1d by BatchNorm2d, for SyncBN purpose"""
    def __init__(self, dim, affine=True):
        super(LinearBatchNorm, self).__init__()
        self.dim = dim
        self.bn = nn.BatchNorm2d(dim, affine=affine)

    def forward(self, x):
        x = x.view(-1, self.dim, 1, 1)
        x = self.bn(x)
        x = x.view(-1, self.dim)
        return x


class SupConResNet(nn.Module):
    """backbone + projection head"""
    def __init__(self, name='resnet50', head='mlp', feat_dim=128, bn_splits=8):
        super(SupConResNet, self).__init__()
        model_fun, dim_in = model_dict[name]
        if model_fun.__name__ == 'efficientnetB2':
            self.encoder = model_fun(bn_splits=bn_splits)
        else:
            self.encoder = model_fun()
        if head == 'linear':
            self.head = nn.Linear(dim_in, feat_dim)
        elif head == 'mlp':
            self.head = nn.Sequential(
                nn.Linear(dim_in, dim_in),
                nn.ReLU(inplace=True),
                nn.Linear(dim_in, feat_dim)
            )
        else:
            raise NotImplementedError(
                'head not supported: {}'.format(head))

    def forward(self, x):
        feat = self.encoder(x)
        feat = F.normalize(self.head(feat), dim=1)
        return feat


class SupCEResNet(nn.Module):
    """encoder + classifier"""
    def __init__(self, name='resnet50', num_classes=10):
        super(SupCEResNet, self).__init__()
        model_fun, dim_in = model_dict[name]
        self.encoder = model_fun()
        self.fc = nn.Linear(dim_in, num_classes)

    def forward(self, x):
        return self.fc(self.encoder(x))


class LinearClassifier(nn.Module):
    """Linear classifier"""
    def __init__(self, name='resnet50', num_classes=10):
        super(LinearClassifier, self).__init__()
        _, feat_dim = model_dict[name]
        self.fc = nn.Linear(feat_dim, num_classes)

    def forward(self, features):
        return self.fc(features)


class SupConMoCo(nn.Module):
    def __init__(self, encoder_name, encoder_head, contrast_mode, dim=128, K=2048, T=0.07, bn_splits=8):
        super(SupConMoCo, self).__init__()

        self.K = K
        self.T = T

        self.contrastive_loss = SupConMoCoLoss(temperature=T, base_temperature=T, contrast_mode=contrast_mode)

        # build encoders
        self.base_encoder = SupConResNet(
            name=encoder_name, 
            head=encoder_head, 
            feat_dim=dim,
            bn_splits=bn_splits
        )
        self.momentum_encoder = SupConResNet(
            name=encoder_name, 
            head=encoder_head, 
            feat_dim=dim,
            bn_splits=bn_splits
        )

        # self._build_projector_and_predictor_mlps(dim, mlp_dim)

        for param_b, param_m in zip(self.base_encoder.parameters(), self.momentum_encoder.parameters()):
            param_m.data.copy_(param_b.data)  # initialize
            param_m.requires_grad = False  # not update by gradient
        
        self.register_buffer("queue", torch.randn(K, 2, dim))
        self.queue = nn.functional.normalize(self.queue, dim=2)

        self.register_buffer("queue_labels", torch.randint(55, (K, )))

        self.register_buffer("queue_ptr", torch.zeros(1, dtype=torch.long))

    @torch.no_grad()
    def _update_momentum_encoder(self, m):
        """Momentum update of the momentum encoder"""
        for param_b, param_m in zip(self.base_encoder.parameters(), self.momentum_encoder.parameters()):
            param_m.data = param_m.data * m + param_b.data * (1. - m)
    
    @torch.no_grad()
    def _dequeue_and_enqueue(self, keys, labels):
        # gather keys before updating queue
        # keys = concat_all_gather(keys)

        batch_size = keys.shape[0]

        ptr = int(self.queue_ptr)
        assert self.K % batch_size == 0  # for simplicity

        # replace the keys at ptr (dequeue and enqueue)
        self.queue[ptr:ptr + batch_size, :, :] = keys
        self.queue_labels[ptr:ptr + batch_size] = labels
        ptr = (ptr + batch_size) % self.K  # move pointer

        self.queue_ptr[0] = ptr

    def forward(self, x, labels, m):
        """
        Input:
            x1: first views of images
            x2: second views of images
            m: moco momentum
        Output:
            loss
        """
        bsz = labels.shape[0]

        # compute features
        base_enc_features = self.base_encoder(x)
        q1, q2 = torch.split(base_enc_features, [bsz, bsz], dim=0)
        base_features = torch.cat([q1.unsqueeze(1), q2.unsqueeze(1)], dim=1)

        with torch.no_grad():  # no gradient
            self._update_momentum_encoder(m)  # update the momentum encoder

            # compute momentum features as targets
            momentum_enc_features = self.momentum_encoder(x)
            k1, k2 = torch.split(momentum_enc_features, [bsz, bsz], dim=0)
            momentum_features = torch.cat([k1.unsqueeze(1), k2.unsqueeze(1)], dim=1)
        
        loss = self.contrastive_loss(
            base_features, 
            momentum_features, 
            self.queue.clone().detach(), 
            labels,
            self.queue_labels.clone().detach()) # + self.contrastive_loss(q2, k1, labels)

        self._dequeue_and_enqueue(
            momentum_features,
            labels
        )

        return loss


class SupConMoCo_CrossDomain(nn.Module):
    def __init__(self, encoder_name, encoder_head, contrast_mode, dim=128, K=2048, T=0.07, bn_splits=8):
        super(SupConMoCo_CrossDomain, self).__init__()

        self.K = K
        self.T = T

        self.contrastive_loss = SupConMoCoLoss(temperature=T, base_temperature=T, contrast_mode=contrast_mode)

        # build encoders
        self.base_encoder = SupConResNet(
            name=encoder_name, 
            head=encoder_head, 
            feat_dim=dim,
            bn_splits=bn_splits
        )
        self.momentum_encoder = SupConResNet(
            name=encoder_name, 
            head=encoder_head, 
            feat_dim=dim,
            bn_splits=bn_splits
        )

        # self._build_projector_and_predictor_mlps(dim, mlp_dim)

        for param_b, param_m in zip(self.base_encoder.parameters(), self.momentum_encoder.parameters()):
            param_m.data.copy_(param_b.data)  # initialize
            param_m.requires_grad = False  # not update by gradient
        
        # sb queue
        self.register_buffer("sb_queue", torch.randn(K, 2, dim))
        self.sb_queue = nn.functional.normalize(self.sb_queue, dim=2)

        self.register_buffer("cv_queue", torch.randn(K, 2, dim))
        self.cv_queue = nn.functional.normalize(self.cv_queue, dim=2)
        
        self.register_buffer("queue_labels", torch.randint(55, (K, )))

        self.register_buffer("queue_ptr", torch.zeros(1, dtype=torch.long))
        


    @torch.no_grad()
    def _update_momentum_encoder(self, m):
        """Momentum update of the momentum encoder"""
        for param_b, param_m in zip(self.base_encoder.parameters(), self.momentum_encoder.parameters()):
            param_m.data = param_m.data * m + param_b.data * (1. - m)
    
    @torch.no_grad()
    def _dequeue_and_enqueue(self, sb_keys, cv_keys, labels):
        # gather keys before updating queue
        # keys = concat_all_gather(keys)

        batch_size = sb_keys.shape[0]

        ptr = int(self.queue_ptr)
        assert self.K % batch_size == 0  # for simplicity

        # replace the keys at ptr (dequeue and enqueue)
        self.sb_queue[ptr:ptr + batch_size, :, :] = sb_keys
        self.cv_queue[ptr:ptr + batch_size, :, :] = cv_keys
        self.queue_labels[ptr:ptr + batch_size] = labels
        ptr = (ptr + batch_size) % self.K  # move pointer

        self.queue_ptr[0] = ptr

    def forward(self, sb_x, cv_x, labels, m, coeff_0=1.0, coeff_1=0.5, coeff_2=0.1):
        """
        Input:
            x1: first views of images
            x2: second views of images
            m: moco momentum
        Output:
            loss
        """
        bsz = labels.shape[0]

        # compute features
        base_enc_features = self.base_encoder(torch.cat((sb_x, cv_x), dim=0))
        sb_q1, sb_q2, cv_q1, cv_q2 = torch.split(base_enc_features, [bsz, bsz, bsz, bsz], dim=0)
        sb_base_features = torch.cat([sb_q1.unsqueeze(1), sb_q2.unsqueeze(1)], dim=1)

        # cv_base_enc_features = self.base_encoder(cv_x)
        # cv_q1, cv_q2 = torch.split(cv_base_enc_features, [bsz, bsz], dim=0)
        cv_base_features = torch.cat([cv_q1.unsqueeze(1), cv_q2.unsqueeze(1)], dim=1)

        with torch.no_grad():  # no gradient
            self._update_momentum_encoder(m)  # update the momentum encoder

            # compute momentum features as targets
            momentum_enc_features = self.momentum_encoder(torch.cat((sb_x, cv_x), dim=0))
            sb_k1, sb_k2, cv_k1, cv_k2 = torch.split(momentum_enc_features, [bsz, bsz, bsz, bsz], dim=0)
            sb_momentum_features = torch.cat([sb_k1.unsqueeze(1), sb_k2.unsqueeze(1)], dim=1)
        
            # cv_momentum_enc_features = self.momentum_encoder(cv_x)
            # cv_k1, cv_k2 = torch.split(cv_momentum_enc_features, [bsz, bsz], dim=0)
            cv_momentum_features = torch.cat([cv_k1.unsqueeze(1), cv_k2.unsqueeze(1)], dim=1)
        

        sb_queue_clone = self.sb_queue.clone().detach()
        cv_queue_clone = self.cv_queue.clone().detach()

        loss = coeff_0 * self.contrastive_loss(
            sb_base_features, 
            sb_momentum_features, 
            sb_queue_clone, 
            labels,
            self.queue_labels.clone().detach()) # + self.contrastive_loss(q2, k1, labels)

        loss += coeff_1 * self.contrastive_loss(
            torch.cat([sb_q1.unsqueeze(1), cv_q1.unsqueeze(1)], dim=1),
            torch.cat([sb_k1.unsqueeze(1), cv_k1.unsqueeze(1)], dim=1),
            torch.cat([sb_queue_clone[:, 0:1, :], cv_queue_clone[:, 0:1, :]], dim=1),
            labels,
            self.queue_labels.clone().detach()
        )

        loss += coeff_2 * self.contrastive_loss(
            cv_base_features, 
            cv_momentum_features, 
            cv_queue_clone, 
            labels,
            self.queue_labels.clone().detach())

        self._dequeue_and_enqueue(
            sb_momentum_features,
            cv_momentum_features,
            labels
        )

        return loss / (coeff_0 + coeff_1 + coeff_2)
