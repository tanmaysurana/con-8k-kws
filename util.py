import random
from torch import nn
import torch
import re


class ZeroPadTransform:
    def __init__(self, target_length=256, random_pad=False):
        self.target_length = target_length
        self.random_pad = random_pad

    def __call__(self, fbank):
        n_frames = fbank.shape[0]
        p = self.target_length - n_frames
        if p > 0:
            # randomly pad at the front and back of the fbank to get fbank of target_length
            if self.random_pad:
                p_offset = random.randint(0, p)
            else:
                p_offset = p // 2
            m = nn.ZeroPad2d((0, 0, p_offset, p - p_offset))
            fbank = m(fbank)
        elif p < 0:
                fbank = fbank[0:self.target_length, :]
        fbank = torch.transpose(fbank, 0, 1)
        fbank = fbank.view(1, fbank.shape[0], fbank.shape[1])
        return fbank


class TwoTransform:
    def __init__(self, transform):
        self.transform = transform

    def __call__(self, x):
        return [self.transform(x), self.transform(x)]


def add_weight_decay_moco_efficientnet(net, l2_value):
    decay = []
    no_decay = []
    for name, param in net.named_parameters():
        if not param.requires_grad: continue # frozen weights
        if name.endswith('.bias'):
            no_decay.append(param)
        elif re.match("^base_encoder[.]encoder[.]features[.][0-9][.][0-9][.]block[.][0-9][.]1[.]weight$", name):
            no_decay.append(param)
        elif name == "base_encoder.encoder.features.0.1.weight":
            no_decay.append(param)
        else:
            decay.append(param)
    return [
            {'params': decay, 'weight_decay': l2_value}, 
            {'params': no_decay, 'weight_decay': 0.0}, 
        ]
