import torchaudio
import torch
from torch.utils.data import Dataset
import torch.nn as nn
import random
import os
import pandas as pd


class UttDataset(Dataset):
    def __init__(self, file_lists, audio_dirs, target_length=256, transform=None, num_mel_bins=128):
        self.file_list =  pd.DataFrame({'file': []})
        for li in range(len(file_lists)):
            df = pd.read_csv(file_lists[li], names=["file"])
            for i in df.index:
                df.at[i, 'file'] = os.path.join(audio_dirs[li], df.iloc[i, 0].split("_")[0], df.iloc[i, 0])
            self.file_list = self.file_list.append(df, ignore_index=True)
        self.label_list = [s for s in os.listdir(audio_dirs[0]) if os.path.isdir(os.path.join(audio_dirs[0], s))]
        self.label_dict = {self.label_list[i]:i for i in range(len(self.label_list))}
        self.target_length = target_length
        self.transform = transform
        self.num_mel_bins = num_mel_bins
    
    def __len__(self):
        return len(self.file_list)
    
    def __getitem__(self, idx):
        audio_path = self.file_list.iloc[idx, 0]
        label = os.path.basename(audio_path).split("_")[0] # file name structure: "<label>_<identifier>"
        wav, sr = torchaudio.load(audio_path)
        
        wav = wav - wav.mean()

        fbank = torchaudio.compliance.kaldi.fbank(
            wav, 
            htk_compat=True, 
            sample_frequency=sr, 
            use_energy=False, 
            window_type='hanning', 
            num_mel_bins=self.num_mel_bins, 
            dither=0.0, 
            frame_shift=10
        )

        if self.transform:
            fbank = self.transform(fbank)
            
        return fbank, self.label_dict[label]


class UttDataset_CrossDomain(Dataset):
    def __init__(self, sb_file_list, sb_audio_dir, cv_file_list, cv_audio_dir, target_length=256, transform=None, num_mel_bins=128):        
        self.sb_file_list = pd.read_csv(sb_file_list, names=["file"])
        for i in self.sb_file_list.index:
                self.sb_file_list.at[i, 'file'] = os.path.join(sb_audio_dir, self.sb_file_list.iloc[i, 0].split("_")[0], self.sb_file_list.iloc[i, 0])
        self.cv_file_list = pd.read_csv(cv_file_list, names=["file"])
        for i in self.cv_file_list.index:
                self.cv_file_list.at[i, 'file'] = os.path.join(cv_audio_dir, self.cv_file_list.iloc[i, 0].split("_")[0], self.cv_file_list.iloc[i, 0])
        self.label_list = [s for s in os.listdir(sb_audio_dir) if os.path.isdir(os.path.join(sb_audio_dir, s))]
        self.label_dict = {self.label_list[i]:i for i in range(len(self.label_list))}
        cv_labels = [self.label_dict[os.path.split(self.cv_file_list.iloc[i, 0])[-1].split("_")[0]] for i in self.cv_file_list.index]
        self.cv_file_list['labels'] = cv_labels
        self.target_length = target_length
        self.transform = transform
        self.num_mel_bins = num_mel_bins

        self.cv_label_indices = [list(self.cv_file_list.loc[self.cv_file_list['labels'] == label].index) for label in range(len(self.label_list))]
        self.cv_label_pointers = [0] * len(self.label_list)
    
    def __len__(self):
        return len(self.sb_file_list)
    
    def __getitem__(self, idx):
        audio_path = self.sb_file_list.iloc[idx, 0]
        label = os.path.basename(audio_path).split("_")[0] # file name structure: "<label>_<identifier>"
        label = self.label_dict[label]
        wav, sr = torchaudio.load(audio_path)
        
        wav = wav - wav.mean()

        sb_fbank = torchaudio.compliance.kaldi.fbank(
            wav, 
            htk_compat=True, 
            sample_frequency=sr, 
            use_energy=False, 
            window_type='hanning', 
            num_mel_bins=self.num_mel_bins, 
            dither=0.0, 
            frame_shift=10
        )

        cv_index = self.cv_label_indices[label][self.cv_label_pointers[label]]
        cv_audio_path = self.cv_file_list.iloc[cv_index]['file']
        self.cv_label_pointers[label] += 1
        if self.cv_label_pointers[label] >= len(self.cv_label_indices[label]):
           self.cv_label_pointers[label] = 0
           random.shuffle(self.cv_label_indices[label]) 
        
        cv_wav, cv_sr = torchaudio.load(cv_audio_path)
        cv_wav = cv_wav - cv_wav.mean()
        cv_fbank = torchaudio.compliance.kaldi.fbank(
                    cv_wav, 
                    htk_compat=True, 
                    sample_frequency=cv_sr, 
                    use_energy=False, 
                    window_type='hanning', 
                    num_mel_bins=self.num_mel_bins, 
                    dither=0.0, 
                    frame_shift=10
                )

        if self.transform:
            sb_fbank = self.transform(sb_fbank)
            cv_fbank = self.transform(cv_fbank)
            
        return [sb_fbank, cv_fbank], label